import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuPage } from './menu.page';

const routes: Routes = [
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: 'menu-tab',
        loadChildren: '../menu-tab/menu-tab.module#MenuTabPageModule'
      },
      {
        path: 'list',
        loadChildren: '../list/list.module#ListPageModule'
      },
      {
        path: 'cards',
        loadChildren: '../cards/cards.module#CardsPageModule'
      },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
