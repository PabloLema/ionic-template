import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  selectedPath = '';
  pages = [
    {
      title: 'Tabs',
      url: '/menu/menu-tab'
    },
    {
      title: 'Lista',
      url: '/menu/list'
    },
    {
      title: 'Tarjetas',
      url: '/menu/cards'
    }
  ];

  constructor(private router: Router) {
    this.router.events.subscribe((event: RouterEvent) => {
      if (event && event.url) {
        this.selectedPath = event.url;
      }
    });
  }

  logOut() {
    this.router.navigate(['/login']);
  }

  ngOnInit() {
  }

}
