import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabTwoPage } from './tab-two.page';

describe('TabTwoPage', () => {
  let component: TabTwoPage;
  let fixture: ComponentFixture<TabTwoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabTwoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabTwoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
