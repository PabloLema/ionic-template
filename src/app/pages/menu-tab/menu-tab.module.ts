import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MenuTabPage } from './menu-tab.page';

const routes: Routes = [
  {
    path: 'menu-tabs',
    component: MenuTabPage,
    children: [
      {
        path: 'tab-one',
        loadChildren: '../tab-one/tab-one.module#TabOnePageModule'
      },
      {
        path: 'tab-two',
        loadChildren: '../tab-two/tab-two.module#TabTwoPageModule'
      },
      {
        path: 'tab-three',
        loadChildren: '../tab-three/tab-three.module#TabThreePageModule'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'menu-tabs/tab-one',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuTabPage]
})
export class MenuTabPageModule {}
