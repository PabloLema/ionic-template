import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab-one',
  templateUrl: './tab-one.page.html',
  styleUrls: ['./tab-one.page.scss'],
})
export class TabOnePage implements OnInit {

  searchResult = '------';
  constructor() { }

  ngOnInit() {
  }

  searchEvent(event) {
    this.searchResult = event.target.value;
    // console.log(event.target.value);
  }

}
