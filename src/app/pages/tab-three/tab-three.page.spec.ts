import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabThreePage } from './tab-three.page';

describe('TabThreePage', () => {
  let component: TabThreePage;
  let fixture: ComponentFixture<TabThreePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabThreePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabThreePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
