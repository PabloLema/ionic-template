import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalPagePage } from '../modal-page/modal-page.page';

@Component({
  selector: 'app-tab-three',
  templateUrl: './tab-three.page.html',
  styleUrls: ['./tab-three.page.scss'],
})
export class TabThreePage implements OnInit {

  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ModalPagePage
    });
    return await modal.present();
  }

}
