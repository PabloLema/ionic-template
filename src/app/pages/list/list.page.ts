import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  scrolled(event) {
    setTimeout(() => {
      event.target.complete();
    }, 2000);
    console.log('scrolled');
  }

}
